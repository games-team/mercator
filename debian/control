Source: mercator
Section: libs
Priority: optional
Maintainer: Debian Games Team <pkg-games-devel@lists.alioth.debian.org>
Uploaders: Olek Wojnar <olek@debian.org>,
           Debian Games Team <team+pkg-games@tracker.debian.org>,
Build-Depends: debhelper-compat (= 13),
               libwfmath-1.0-dev (>= 1.0.2+dfsg1-0.4),
               pkg-config,
Standards-Version: 4.5.0
Rules-Requires-Root: no
Homepage: https://www.worldforge.org/
Vcs-Git: https://salsa.debian.org/games-team/mercator.git
Vcs-Browser: https://salsa.debian.org/games-team/mercator

Package: libmercator-0.3-4
Architecture: any
Multi-Arch: same
Pre-Depends: ${misc:Pre-Depends}
Depends: ${misc:Depends}, ${shlibs:Depends}
Description: WorldForge terrain library
 Mercator is primarily aimed at terrain for multiplayer online games and
 forms one of the WorldForge core libraries.
 It is intended to be used as a terrain library on the client, while a
 subset of features are useful on the server.
 .
 Mercator is designed in such a way that individual tiles can be
 generated on-the-fly from a very small source data set.  Each tile uses
 a fast deterministic random number generation to ensure that identical
 results are produced "anytime, anywhere".  This enables transmission of
 terrain across low bandwidth links as part of the standard data stream,
 or server side collision detection with the same terrain that the
 player sees.
 .
 The use of tiles means that there is inherently a large degree of gross
 control of the shape of the terrain.  Finer control is implemented by
 allowing geometric modifications - for example, a polygonal area might
 be flattened, or a crater could be applied.

Package: libmercator-0.3-dev
Section: libdevel
Architecture: any
Multi-Arch: same
Depends: libmercator-0.3-4 (= ${binary:Version}),
         libwfmath-1.0-dev (>= 1.0.2+dfsg1-0.4),
         ${misc:Depends}
Description: WorldForge terrain library - development files
 Mercator is primarily aimed at terrain for multiplayer online games and
 forms one of the WorldForge core libraries.
 It is intended to be used as a terrain library on the client, while a
 subset of features are useful on the server.
 .
 Mercator is designed in such a way that individual tiles can be
 generated on-the-fly from a very small source data set.  Each tile uses
 a fast deterministic random number generation to ensure that identical
 results are produced "anytime, anywhere".  This enables transmission of
 terrain across low bandwidth links as part of the standard data stream,
 or server side collision detection with the same terrain that the
 player sees.
 .
 The use of tiles means that there is inherently a large degree of gross
 control of the shape of the terrain.  Finer control is implemented by
 allowing geometric modifications - for example, a polygonal area might
 be flattened, or a crater could be applied.
 .
 This package contains the files for developing with the mercator library.
